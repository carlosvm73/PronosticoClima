export let GeoModel = (geo = undefined)=>{
    return {
        pais: (geo === undefined) ? "Perú" : geo.pais,
        departamento: (geo === undefined || geo.departamento === undefined) ? "" : geo.departamento,
        provincia: (geo === undefined || geo.provincia === undefined) ? "" : geo.provincia,
        distrito: (geo === undefined || geo.distrito === undefined) ? "" : geo.distrito
    }
}

export let PlacesModel = (places = undefined)=>{
    return {
        pais: (places === undefined || places.pais === undefined) ? "" : places.pais,
        departamento: (places === undefined || places.departamento.nombre === undefined) ? "" : places.departamento.nombre,
        provincia: (places === undefined || places.provincia.nombre === undefined) ? "" : places.provincia.nombre,
        distrito: (places === undefined || places.distrito.nombre === undefined) ? "" : places.distrito.nombre
    }
}

export let ObjForecastModel =  (obj = undefined)=>{
    return {
        astronomy: { sunrise: "", sunset: "" },
        atmosphere: { humidity: "", pressure: "", rising: "", visibility: "" },
        description: "",
        image: { title: "", width: "", height: "", link: "", url: "" },
        item: {
            title: "", lat: "", long: "", link: "", pubDate: "",
            condition: { code: "", date: "", temp: "", text: "" },
            guid: { isPermaLink: "" },
            forecast: []
        },
        language: "",
        lastBuildDate: "",
        link: "",
        location: { city: "", country: "", region: "" },
        title: "",
        ttl: "",
        units: { distance: "", pressure: "", speed: "", temperature: "" },
        wind: { chill: "", direction: "", speed: "" }
    }
}
export let  ForecastModel = (forecast = undefined)=>{
    return {
        code: "",
        date: "",
        day: "",
        high: "",
        low: "",
        text: ""
    }
} 