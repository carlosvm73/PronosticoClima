import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retry';
import { GLOBAL } from "./global.service";

@Injectable()
export class ServicesService {
  public url: string;
  constructor(private _http: Http) { 
    this.url = GLOBAL.url;
  }
  private headers = new Headers({'Content-type': 'application/json'});

  getPlace(): Observable<any> {
    return this._http.get("../assets/ubigeo-peru.json")
    // return this._http.get("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20geo.places%20where%20text%3D%22lima%20peru%22&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys")
    .map(res=>{
      return res.json();
    })
    .catch((err: any)=> void{
      
    });
  };

  getPronostico(data): Observable<any> {
    // return this._http.get("ubigeo-peru.json")
    return this._http.get(`https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22${data.distrito}%2C%20${data.provincia}%2C%20${data.departamento}%2C%20${data.pais}%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys`)
      .map(res => {
        return res.json();
      })
      .catch((err: any) =>  {
        return err.json();
      });
  };
}

// https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20geo.places%20where%20text%3D%22lima%20peru%22&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys