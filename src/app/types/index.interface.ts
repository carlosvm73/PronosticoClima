export interface place {
    pais: string,
    departamento: string,
    provincia: string,
    distrito: string
}
export interface placeItem {
    departamento: string,
    provincia: string,
    distrito: string,
    nombre: string
}
export interface ObjForecast {
    astronomy: { sunrise: string, sunset: string },
    atmosphere: { humidity: string, pressure: string, rising: string, visibility: string }
    description: string,
    image: { title: string, width: string, height: string, link: string, url: string },
    item: {
        title: string, lat: string, long: string, link: string, pubDate: string,
        condition: { code: string, date: string, temp: string, text: string },
        guid: { isPermaLink: string },
        forecast: Array<any>
    },
    language: string,
    lastBuildDate: string,
    link: string,
    location: { city: string, country: string, region: string },
    title: string,
    ttl: string,
    units: { distance: string, pressure: string, speed: string, temperature: string },
    wind: { chill: string, direction: string, speed: string }
}
export interface Forecast {
    code: string,
    date: string,
    day: string,
    high: string,
    low: string,
    text: string
}