import { Component, OnInit } from '@angular/core';

import { ServicesService } from '../services.service';
import { Forecast, ObjForecast, place, placeItem } from '../types/index.interface';
import { GeoModel, PlacesModel, ObjForecastModel, ForecastModel  } from '../models/index.model';

@Component({
  selector: 'app-pronostico',
  templateUrl: './pronostico.component.html',
  styleUrls: ['./pronostico.component.css']
})
export class PronosticoComponent implements OnInit {

  constructor(private _service: ServicesService) { }

  private forecast: ObjForecast = ObjForecastModel();
  private regions: any = [];
  private cities:any=[];
  private districs:any=[];

  private geo:place = PlacesModel();
  

  ngOnInit() {
    this._service.getPlace().subscribe(response =>{
      this.regions = response.filter((value: placeItem, key)=>{
        return value.provincia == "00" && value.distrito == "00"
      });
    }, error=>{
      console.log(error);
      });
  }
  selectCountry(selected){
    this._service.getPlace().subscribe(response =>{
      this.regions = response.filter((value: placeItem, key)=>{
        return value.provincia == "00" && value.distrito == "00"
      });
    }, error=>{
      console.log(error);
      });
  }
  selectedRegion(selected) {
    this._service.getPlace().subscribe(response => {
      this.cities = response.filter((value: placeItem, key) => {
        return value.departamento == selected.departamento && value.provincia != "00" && value.distrito == "00";
      });
    }, error => {
      console.log(error);
    });
  }

  selectedDistrict(selected) {
    this._service.getPlace().subscribe(response => {
      this.districs = response.filter((value: placeItem, key) => {
        return value.provincia == selected.provincia && value.departamento == selected.departamento;
      });
    }, error => {
      console.log(error);
    });
  }

  generate(geo){
    let places: place = PlacesModel(geo);
    console.log(places);
    this._service.getPronostico(places).subscribe(response => {
      console.log(response);
      if (response.query.count > 0){
        console.log(response.query.results.channel);
        this.forecast = response.query.results.channel;
      }else{
        alert("No hay resultados.");
      }
    }, error => {
      console.log(error);
    });
  }
  

}

